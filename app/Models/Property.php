<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'title',
        'price',
        'status_id',
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function setPriceAttribute($value)
    {
    	$value = str_replace(' ','', $value);
    	$value = str_replace(',','.', $value);
    	$this->attributes['price'] = number_format((double) $value, 2, '.', '');
    }

    public function getPriceAttribute($value)
    {
    	return number_format($value, 2, ',','.');
    }
}
