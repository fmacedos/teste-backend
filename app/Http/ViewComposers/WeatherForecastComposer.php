<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class WeatherForecastComposer
{
    /**
     * The user repository implementation.
     *
     * @var Climatempo
     */
    protected $weather;

    public function __construct()
    {

        $this->weather =  app()->make('AdinanCenci\Climatempo\Climatempo', [ 'token' =>getenv('CLIMATEMPO_TOKEN') ]);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('weather', $this->weather->current(getenv('CLIMATEMPO_CITY')));
    }
}
