<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use AdinanCenci\Climatempo\Climatempo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->climaTempoCookie();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $properties = Property::with('status')->get();
        $feed = $this->feed();
        return view('dashboard', compact('properties','feed'));
    }

    private function feed()
    {
        $feed = \Feeds::make("https://exame.abril.com.br/noticias-sobre/mercado-imobiliario/feed/", 2);
        return $feed->get_items();
    }

    private function climaTempoCookie()
    {
        if(!\Cookie::has('climatempo_temperatura'))
        {
            $climatempo = new Climatempo(getenv('CLIMATEMPO_TOKEN'));
            $climatempo = $climatempo->current(getenv('CLIMATEMPO_CITY'));
            \Cookie::queue(\Cookie::make('climatempo_temperatura', $climatempo->data->temperature));
        }
    }
}
