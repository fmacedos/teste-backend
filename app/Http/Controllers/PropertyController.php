<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Models\Status;
use App\Http\Requests\Property as PropertyForm;
use Illuminate\Http\Request;



class PropertyController extends Controller
{
    private $model;

    public function __construct(Property $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = $this->model->paginate(10);
        return view('properties.index', compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::all()->sortBy("status");
        return view('properties.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertyForm $request)
    {
        $save = Property::create($this->prepareModel($request->all()));
        if ($save) {
            return redirect()->route('properties.index')->with('success', trans('messages.properties.created'));
        }

        return back()->withInput()->withError($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = Property::find($id);
        if(!$property) {
            return redirect()->route('properties.index')->with('errors', trans('messages.properties.notfound'));
        }
        $status = Status::all()->sortBy("status");
         return view('properties.edit',compact('property','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PropertyForm $request, $id)
    {
        $property = Property::find($id);
        if(!$property) {
            return redirect()->route('properties.index')->with('errors', trans('messages.properties.notfound'));
        }

        $save = $property->update($this->prepareModel($request->all()));
        if ($save) {
            return redirect()->route('properties.index')->with('success', trans('messages.properties.updated'));
        }

        return back()->withInput()->withError($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::find($id);
        if(!$property) {
            return redirect()->route('properties.index')->with('errors', trans('messages.properties.notfound'));
        }
        
        $property->delete();
        return redirect()->route('properties.index')->with('success', trans('messages.properties.deleted'));
    }

    private function prepareModel($data = [])
    {
        return [
            'title' => $data['title'],
            'price' => $data['price'],
            'status_id' => $data['status_id'],
        ];
    }
}
