<?php

return [
	'properties.created' => 'Imóvel cadastrado com sucesso',
	'properties.updated' => 'Imóvel alterado com sucesso',
	'properties.deleted' => 'Imóvel excluído com sucesso',
	'properties.notfound' => 'Imóvel não encontrado',
];