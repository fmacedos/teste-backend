<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'As informações de login não foram encontradas.',
    'throttle' => 'Muitas tentativas de login. Por favor tente novamente em :seconds segundos.',
    'login-header' => 'Entrar no sistema',
    'label-email' => 'Endereço de E-mail',
    'label-password' => 'Senha',
    'label-remember' => 'Lembrar e-mail e senha',
    'label-forgot' => 'Esqueci e-mail e/ou senha',
    'label-login' => 'Entrar',
    'label-register' => 'Registar',
    'label-logout' => 'Sair',
    'btn-login' => 'Acessar',

];