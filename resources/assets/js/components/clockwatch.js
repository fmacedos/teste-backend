module.exports.ticktack = function() {
	let clock = document.getElementById('clockwatch-box');
	let date = new Date();
	let digits = function(digit) {
		return digit < 10 ? '0' + digit : digit;
	}

	let hours = digits(date.getHours());
	let minutes = digits(date.getMinutes());
	clock.innerText = [hours, minutes].join(':');

}