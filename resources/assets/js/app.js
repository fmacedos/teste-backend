require('./bootstrap');

let clockwatch = require('./components/clockwatch');
import mask from 'simple-mask-money';

//clockwatch
clockwatch.ticktack();
setInterval(clockwatch.ticktack, 1000);


//money mask
// configuration
const options = {
	prefix: '',
	suffix: '',
	fixed: true,
	fractionDigits: 2,
	decimalSeparator: ',',
	thousandsSeparator: '.',
	autoCompleteDecimal: true
};

// set mask on your input you can pass a querySelector or your input element and options
//let input = mask.SimpleMaskMoney.setMask('.money', options);


//delete button

$('.delete').click(function(){
	if(confirm('Deseja excluir este imóvel?')) {
		return true;
	}
	return false;
});
