<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-1 col-xs-3 col-md-3">
                <span id="clockwatch-box">00:00</span>
            </div>
            <div class="col-lg-10 col-xs-9 col-md-9">
                <span class="weather-box">
                    <span class="weather-box__label">São Paulo  </span> - 
                    <span class="weather-box__temperature">
                    {{ \Cookie::get('climatempo_temperatura') }} °C</span>
                </span>
                <a class="footer__brand pull-right" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>
        </div>
    </div>
</footer>