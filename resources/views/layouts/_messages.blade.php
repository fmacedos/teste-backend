
<div class="container-fluid">
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <p>@lang($message)</p>
    </div>
    @endif

    @if (isset($errors) && count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @if(is_string($errors))
                <li>{{ $errors }} </li>
            @else
                @foreach ($errors->all() as $key => $error)
                <li>{{ $error }}</li>
                @endforeach
            @endif
        </ul>
    </div>
    @endif
</div>