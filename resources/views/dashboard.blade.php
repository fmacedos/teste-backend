@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')

<div class="panel-body">
	<h1> Dashboard
		<div class="pull-right">
			<a href="{{ route('properties.index') }}" class="btn btn-default" title="Gestão de imóveis"><i class="glyphicon glyphicon-list"></i> Imóveis</a>
		</div>
		<hr>
	</h1>

	<div class="row">
		<div class="col-xs-12 col-md-3 col-lg-3 border-right">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="small-box bg-red" title="Total de Imóveis já vendidos">
						<div class="inner">
							<h3>{{ $properties->where('status_id', 3)->count() }}</h3>
							<p>Imóveis Vendidos</p>
						</div>
						<div class="icon">
							<i class="glyphicon glyphicon-briefcase"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="small-box bg-yellow" title="Total de Imóveis que estão alugados">
						<div class="inner">
							<h3>{{ $properties->where('status_id', 2)->count() }}</h3>
							<p>Imóveis Alugados</p>
						</div>
						<div class="icon">
							<i class="glyphicon glyphicon-link"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="small-box bg-acqua" title="Total de Ímóveis disponíveis para compra">
						<div class="inner">
							<h3>{{ $properties->where('status_id', 1)->count() }}</h3>
							<p>Imóveis Disponíveis</p>
						</div>
						<div class="icon">
							<i class="glyphicon glyphicon-home"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-9 col-lg-9">
				<div class="container-fluid">
				  @foreach ($feed as $item)
				    <div class="item">
				      <h4><a href="{{ $item->get_permalink() }}" target="_blank" title="{{ $item->get_title() }}">{{ $item->get_title() }}</a></h4>
				      <div class="row">
				      	<div class="col-xs-8 col-md-8 col-lg-8">
				      		<p>{{ $item->get_description() }}</p>
				      		<p><small>Postado {{ $item->get_date('j/m/Y | g:i a') }}</small></p>
				      	</div>
				      	<div class="col-xs-4 col-md-4 col-lg-4">
				      		<img src="{{ $item->get_thumbnail()['url'] }}" height="70" alt="{{ $item->get_title() }}">
				      	</div>
				      </div>
				    </div> <hr>
				    @break($loop->index == 2)
				  @endforeach
				</div>
		</div>
	</div>
</div>
@endsection
