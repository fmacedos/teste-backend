@extends('layouts.app')

@section('title', 'Alterar Imóvel')

@section('content')
<div class="panel-body">
	<h1>Alterar Imóvel: {{ $property->title }}
	 <div class="pull-right">
        <a href="{{ route('dashboard') }}" class="btn btn-primary"><i class="glyphicon glyphicon-list-alt"></i> Dashboard</a>
        <a href="{{ route('properties.index') }}" class="btn btn-default"><i class="glyphicon glyphicon-list"></i> Imóveis</a>
      </div>
	 <hr></h1>

	<form action="{{ route('properties.update', ['id' => $property->id])}}" method="post" enctype="multipart/form-data">
		{{ method_field('PUT') }}
		@include('properties._property')
	</form>
</div>
@stop
