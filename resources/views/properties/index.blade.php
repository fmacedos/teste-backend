@extends('layouts.app')

@section('title', 'Gestão de Imóveis')

@section('content')
<div class="panel-body">
    <h1>
      Gestão de Imóveis
      <div class="pull-right">
        <a href="{{ route('dashboard') }}" class="btn btn-primary" title="Ir para Dashboard"><i class="glyphicon glyphicon-list-alt"></i> Dashboard</a>
        <a href="{{ route('properties.create') }}" class="btn btn-success" title="Adicionar novo imóvel"><i class="glyphicon glyphicon-plus"></i> Adicionar</a>
      </div>
      <hr>
    </h1>
    <div class="table-responsive">
      <table class="table table-hover">
        <tr>
          <th class="col-xs-1">id</th>
          <th class="col-xs-5">titulo</th>
          <th class="col-xs-2">valor</th>
          <th class="col-xs-2">situação</th>
          <th class="col-xs-4">&nbsp;</th>
        </tr>
        @foreach ($properties as $property)
        <tr>
          <td>{{ $property->id }}</td>
          <td>{{ str_limit($property->title, 150, '...') }}</td>
          <td>R$ {{ $property->price }}</td>
          <td>{{ $property->status->status }}</td>
          <td>
            <a href="{{ route('properties.edit',[$property->id]) }}" class="btn btn-default btn-sm" title="Alterar imóvel">
              <i class="glyphicon glyphicon-pencil"></i> editar
            </a>
            <a href="{{ route('properties.delete',[$property->id]) }}" class="btn btn-danger btn-sm delete" title="Excluir imóvel">
              <i class="glyphicon glyphicon-trash"></i> excluir
            </a>
          </td>
        </tr>
        @endforeach
      </table>
    </div>
    <div class="text-center">
      <hr>
      {{ $properties->links() }}
  </div>
  </div>
@endsection