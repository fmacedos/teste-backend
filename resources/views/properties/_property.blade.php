{{ csrf_field() }}
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="form-group">
      <label for="">Título</label>
      <input type="text" class="form-control" required name="title" value="{{ $property->title or '' }}">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12 col-md-8 col-lg-8">
    <div class="form-group">
      <label for="">Valor</label>
      <input type="text" maxlength="15" class="form-control money" required name="price" value="{{ $property->price or '' }}">
    </div>
  </div>
  <div class="col-xs-12 col-md-4 col-lg-4">
    <div class="form-group">
      <label for="">Situação</label>
      <select name="status_id" class="form-control">
        <option value="1">----</option>
        @foreach($status as $status_id)
        <option 
        @if(isset($property->status_id) && $status_id->id == $property->status_id) selected @endif
        value="{{ $status_id->id }}">
        {{ $status_id->status }}
      </option>
      @endforeach
    </select>
  </div>
</div>
</div>
<div class="row">
  <div class="col-xs-12 col-lg-12">
    <label for="">&nbsp;</label>
    <button type="submit" class="btn btn-success btn-block">
      <i class="fa fa-save"></i>
      salvar
    </button>
  </div>
</div>
