<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert(['id' => 1, 'status' => 'disponível']);
        DB::table('status')->insert(['id' => 2, 'status' => 'alugado']);
        DB::table('status')->insert(['id' => 3, 'status' => 'vendido']);
    }
}
