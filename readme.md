# Teste vaga backend

Projeto desenvolvido para avaliação de vaga de desenvolvedor backend.
Autor [Fernando Macedo dos Santos](mailto:fernando@foormes.com.br)
17/02/2018

## Instalação
1) Faça o clone no [Bitbucket](https://bitbucket.org/fmacedos/teste-backend)
2) Rode no terminal: **composer install** para instalar as dependências do backend
3) Rode no terminal: **npm install** para instalar as dependências do frontend
4) Verifique se existe o arquivo .env, se existir configure os dados do banco de dados:

> DB_HOST=< HOST >
> DB_PORT=< PORTA >
> DB_DATABASE=< DATABASE >
> DB_USERNAME=< USERNAME >
> _PASSWORD=< PASSWORD >

5) Rode no terminal: **php artisan migrate** para criar toda a estrutura de dados
6) Rode no terminal: **php artisan db:seed** para criar os primeiros registros
7) Rode no terminal: ** npm run watch ** para as dependências do Frontend
8) Rode no terminal: **php artisan serve** para rodar o Webserver do projeto
9) Abra o navegador e digite na barra de endereço: **http://127.0.0.1:8000**, pronto aplicação está rodando
10) Para acessar o sistema entre com as credencias:
> email: admin@email.br
> senha: admin

11) Agora, pode utilizar

## Dependências Backend
* PHP >= 7.0
* Mysql 5.7
*  Apache >= 2.4.27
*  [Laravel 5.5](https://laravel.com)
* [ClimaTempoAPI](https://github.com/adinan-cenci/climatempo-api)
* [Laravel Feeds](https://github.com/willvincent/feeds)


## Dependências Frontend
* Npm >= 3.5.2
* [BootstrapSass](https://github.com/twbs/bootstrap-sass)
* [jQuery](https://jquery.com)
* [LaravelMix](https://github.com/JeffreyWay/laravel-mix)
* [Lodash](https://lodash.com)
* [SimpleMaskMoney](https://github.com/codermarcos/simple-mask-money)